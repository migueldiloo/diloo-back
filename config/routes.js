/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'get /'                      :{view:'init'},
  'get /login'                 : {view:'index'},
  'get /operator/home'         : 'OperatorController.home',
  'get /operator/history'      : 'OperatorController.historial',

  'get /operator/agent'        : {view:'operator_agents'}, //view
  'post /operator/create'      : 'OperatorController.create',
  'post /operator/update'      : 'OperatorController.updateProfile',
  'get /operator/getAll'       : 'OperatorController.getAll',
  'post /operator/getOperator' : 'OperatorController.getOperator',  
  'post /operator/login'       : 'OperatorController.login',  
  'post /operator/resetPasswd' : 'OperatorController.resetPasswd', 

  'get /logout'                : 'AuthController.logout',

  'get  /user/register'        : 'TmpuserController.registerUser',
  'get  /user/validate'        : 'TmpuserController.validateUser',
  'get  /user/login'           : 'UsersController.login',
  'get  /user/create'          : 'UsersController.create',
  'get  /user/getUserInfo'     : 'UsersController.getUserInfo',
  'post /user/upload'          : 'UsersController.uploadPhoto',

  'get  /ticket/create'        : 'TicketController.review',
  'get  /ticket/getMyTickets'  : 'TicketController.getMyTickets',
  'get  /ticket/closeTicket'   : 'TicketController.closeTicket',
  'get  /ticket/join'          : 'TicketController.join',
  'get  /ticket/getAreaTickets': 'TicketController.getAreaTickets',
  'get  /ticket/closeTicket'   : 'TicketController.closeTicket', 
  'get  /ticket/getCloseds'    : 'TicketController.getClosedTicketsInArea', 
  'get  /ticket/gmft'          : 'TicketController.getMessagesFromTicket', 
  'get  /ticket/findByUser'    : 'TicketController.getClosedTicketsInAreaByUserName', 


  'get /message/movil'         : 'MessageController.postMessage',
  'get /message/web'           : 'MessageController.sendMessage',
  'get /message/join'          : 'MessageController.join',
  'get /message/gmft'          :  'MessageController.gmft'
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
