'use strict';
var chatWindow={
  getDom:function(){
    var dom=new Array(document.querySelectorAll("[data-action='minimize']"));
    return dom;
  },
  toggleIcon:function($el){
    var $attrIcon=$el.getAttribute('data-icon');
    if($attrIcon === 'minimize'){
      $el.setAttribute('data-icon','maximize');
    }else{
      $el.setAttribute('data-icon','minimize');
    }
  },
  toggleAction:function($el){
    var $attrIcon=$el.getAttribute('data-action');
    if($attrIcon === 'minimize'){
      $el.setAttribute('data-icon','maximize');
    }else{
      $el.setAttribute('data-icon','minimize');
    }
  },
  action:function($el){
    $el.addEventListener('click',function(){
      this.toggleAction($el);
      this.toggleIcon($el);
    });
  },
  init:function(){
    var $dom=this.getDom();
    $dom.forEach(function($el){
      this.action($el);
    });
  }
};
