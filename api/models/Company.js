/**
* Company.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName:'companies',
  connection:'mySQL',  
  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true,
      autoIncrement:true
    },
    name:{
      type:'string',
      maxLength:60,
      minLength:3,
      unique:true,
      required:true
    },
    image:{
      type:'string',
      unique:true,
      required:true,
      notNull:true
    },
    bgImage:{
      type:'string',
      required:true,
      notNull:true
    },
    description:{
      type:'string',
      required:true,
      notNull:true
    },
    category:{
      model:'category'
    },
    areas:{
      collection:'area',
      via:'companies'
    },
    //employes
    supervisors:{
      collection:'supervisor',
      via:'company'
    },
    operators:{
      collection:'operator',
      via:'company'
    },
    status:{
      type:'boolean',
      defaultsTo:true
    }
  }
};
