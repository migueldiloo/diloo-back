/**
* Message.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	connection:'mySQL',
	attributes: {
		//area    :'integer',
		author  :'string',
		body    :'string',
		ticket  :'integer',
		type    :'string'
		//company :'integer'
	}
};

