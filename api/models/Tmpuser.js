/**
* Tmpuser.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {
  connection:'mySQL',	
	attributes: {
		email_login:{
			type:'email',
			maxLength:40,
			minLength:5,
			required:true,
			notNull:true
		},
		tmp_code:{
			type:'string',
			notNull:true
		}
	}
};

