/**
* Chat.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName:'chats',
  connection:'mySQL',  
  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true,
      autoIncrement:true
    },
    mensaje:{
      type:'string',
      required:true,
      notNull:true
    },
    type:{
      type:'string',
      required:true,
      notNull:true,
      in:['text','photo','file']
    },
    ticket:{
      model:'ticket'
    }
  }
};
