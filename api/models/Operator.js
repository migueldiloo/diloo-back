/**
* Operator.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt-nodejs');
module.exports = {
  tableName:'operators',
  connection:'mySQL',  
  attributes: {
    firstName:{
      type:'string'
    },
    lastName:{
      type:'string'
    },
    displayName:{
      type:'string'
    },
    email:{
      type:'string',
      maxLength:40,
      required:true,
      unique:true
    },
    password:{
      type:'string',
      defaultsTo:'123456'
    },
    company:{
      model:'company',
      notNull:true,
      required:true
    },
    status:{
      type:'boolean',
      defaultsTo:true
    },
    areas:{
      collection:'area',
      via:'operators'
    },
    toJSON:function(){
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  }
};
