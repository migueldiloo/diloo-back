/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
module.exports = {
  tableName:'users',
  connection:'mySQL',  
  schema: true,
  autoPK: true,
  attributes: {
    email_login:{
      type:'email',
      maxLength:40,
      minLength:5,
      required:true,
      unique:true,
      notNull:true
    },
    password:{
      type:'string',
      required:true,
      maxLength:100,
      minLength:6,
      notNull:true
    },
    lat:{
      type:'float',
      required:true,
      notNull:true
    },
    lng:{
      type:'float',
      required:true,
      notNull:true
    },
    person:{
      model:'person',
      notNull:true
    },
    status:{
      type:'boolean',
      defaultsTo:true
    },
    tickets:{
      collection:'ticket',
      via:'user'
    },
    toJSON:function(){
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  }
};
