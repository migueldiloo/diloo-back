/**
* Category.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName:'categories',
  connection:'mySQL',  
  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true,
      autoIncrement:true
    },
    name:{
      type:'string',
      maxLength:60,
      minLength:3,
      unique:true,
      required:true
    },
    companies:{
      collection:'company',
      via:'category'
    },
    status:{
      type:'boolean',
      defaultsTo:true
    }
  }
};
