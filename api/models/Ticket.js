/**
* Ticket.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName:'tickets',
  connection:'mySQL',  
  attributes: {
    status:{
      type:'integer',
      defaultsTo:0
    },
    user:{
      model:'users',
      required:true
    },
    company:{
     // model:'company',
     type:'integer',
      required:true,
      notNull:true
    },
    area:{
      //model:'area',
      type:'integer',
      //required:true,
      //notNull:true,
      defaultsTo:1
    },
    operator:{
      type:'integer',
      defaultsTo:0
    }
  }
};
