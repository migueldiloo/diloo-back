/**
* Area.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName:'areas',
  connection:'mySQL',
  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true,
      autoIncrement:true
    },
    name:{
      type:'string',
      maxLength:30,
      minLength:3,
      unique:true,
      required:true
    },
    companies:{
      collection:'company',
      via:'areas'
    },
    status:{
      type:'boolean',
      defaultsTo:true
    },
    operators:{
      collection:'operator',
      via:'areas'
    }/*,
    ticktes:{
      collection:'ticket',
      via:'area'
    }*/
  }
};
