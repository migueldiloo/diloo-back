/**
* Supervisor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt-nodejs');
module.exports = {
  tableName:'supervisors',
  connection:'mySQL',  
  attributes: {
    id:{
      type:'integer',
      unique:true,
      primaryKey:true,
      autoIncrement:true
    },
    email_login:{
      type:'email',
      maxLength:40,
      minLength:10,
      required:true,
      unique:true,
      notNull:true
    },
    password:{
      type:'string',
      required:true,
      maxLength:100,
      minLength:6,
      notNull:true
    },
    person:{
      model:'person',
      unique:true,
      notNull:true
    },
    company:{
      model:'company',
      notNull:true,
      required:true
    },
    status:{
      type:'boolean',
      defaultsTo:true
    },
    toJSON:function(){
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  }/*,
    beforeCreate : function(user,cb){
      bcrypt.genSalt(10,function(err,salt){
        bcrypt.hash(user.password,salt,function(err,hash){
          if(err){
            console.log(err);
            cb(err);
          }else{
            user.password = hash;
            cb();
          }
        });
      });
    }    */

};
