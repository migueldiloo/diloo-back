/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var AWS = require('aws-sdk');
 AWS.config.loadFromPath('./s3_config.json')
 var s3 = new AWS.S3({params:{Bucket:'diloo'}});

var bcrypt = require('bcrypt-nodejs');
var path = require('path');
module.exports = {
	/*_config:{
		actions:false,
		shortcuts:false,
		rest:false
	},*/
	getUserInfo:function(req,res){
		//if(req.signedCookies.signin){
			Users.findOne({id:req.param('userId')}).exec(function(err,obj){
				if(err){
					res.json({err:err,success:false});
				}else if(obj){
					Person.findOne({id:obj.person}).exec(function(err2,obj2){
						if(err2){
							res.json({err:err2,success:false});
						}else if(obj2){
							var response = {
								nombres : obj2.first_name + ' ' + obj2.first_lastname,
								email   : obj.email_login,
								id      : obj.id,
								photo   : obj2.photo,
								phone   : obj2.phone
							}
							//obj.personInfo = obj2;
							res.json({err:null,success:true,user:response});
						}else{
							res.json({err:null,success:false,message:"no se han encontrado personas"});
						}
					});
				}else{	
					res.json({err:null,success:false,message:"no se han encontrado resultados"});
				}
			});
		/*}else{
			res.json({err:'No registrado',success:false});
		}*/
	},
	sendPassword:function(req,res){
		if (!req.param('email_login')){
			res.json({err:'se esperaba un correo',success:false});
		}else{
			Users.findOne({'email_login':req.param('email_login')}).exec(function cb(err,user){
				if(err){
					res.json({err:err,success:false});
				}else if(user){
					//enviar mail
					NodeMailer(user.email_login,user.password,function(nmerror,nminfo){
						if(nmerror){
							res.json({err:nmerror,success:false});
						}else if(nminfo){
							res.json({err:null,success:true});
						}
					})
				}else{
					res.json({err:null,success:false});
				}
			});
		}
	}
	,
	create:function(req,res){
		var id ;
		function set(newId){
			id = newId;
		}
		var newperson = req.allParams();
		//newperson.photo = files[0].fd;
		//console.log(newperson);
		Person.create(newperson).exec(function cb(err,person){
			if(err){
				res.json({error:err,success:false});
			}else{
				var data = req.allParams();
				data.person = person.id;
				if(data.password.length > 20){
					res.json({
						err:"longitud maxima de 20 caracteres",success:false
					});
				}else{
					data.password=bcrypt.hashSync(data.password);
					Users.create(data).exec(function cb(err2,obj2){
						if(err2){
							res.json({err:err2,success:false});
						}else{
							res.json({err:null,success:true,person:person,user:obj2});
						}
					});					
				}
			}
		});
	},
	uploadPhoto:function(req,res){
		if( req.param('photo') && req.param('personId') && req.param('photo').length > 20 ){
			buf = new Buffer(req.param('photo').replace(/^data:image\/png;base64,/, ""),'base64');
			var data = {
				Key : req.param('personId')+'.jpg',
				Body: buf,
				ContentEncoding:'base64',
				ContentType : 'image/jpg'
			}
			s3.putObject(data,function(err,data){
				if(err){
					res.json({error:err,success:false})
				}else{
					Person.update({id:req.param('personId')},{photo: ('https://s3-sa-east-1.amazonaws.com/diloo/'+req.param('personId')+'.jpg')})
						.exec(function(err2,obj){
						if(err2){
							res.json({err:err2,success:false});
						}else if(obj.length){
							Users.findOne({person:obj[0].id}).exec(function(eus,us){
								if(eus){
									res.json({err:eus,success:false});
								}else{
									//console.log(obj);
									//console.log(us)
									obj[0].user=us;
									res.json({err:null,success:true,person:obj});
								}
							})
							
						}else{
							res.json({err:'no se ha encontrado un usuario con ese id',success:true,person:null});
						}
					});						
				}
			})
		}else{
			res.json({err:"parametros invalidos",success:false});
		}
	},
	login:function(req,res){
		if(req.param('email_login') && req.param('passwd') ){
			var mail = req.param('email_login');
			var pass = req.param('passwd');
			Users.findOne({email_login:mail}).exec(function(err,user){
				if(err){
					res.json({err:err,success:false});
				}else{
					if(user && bcrypt.compareSync(pass,user.password)){
						Person.findOne({id:user.person}).exec(function(errp,person){
							if(errp){
								res.json({err:errp,success:false});
							}else if(person){
								person.userId=user.id
								person.email_login=user.email_login;
								person.photo = person.photo;
								person.name = person.first_name;
								res.cookie('signin','true',{signed:true});
								res.json({err:null,success:true,person:person});
							}else{
								res.json({err:null,success:true,person:null});
							}
						});
					}else{
						res.json({err:'Usuario o constraseña invalida',success:false});
					}
				}
			});
		}else{
			res.json({err:'Parametros invalidos',success:false});
		}
	}

};

