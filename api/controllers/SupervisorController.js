/**
 * SupervisorController
 *
 * @description :: Server-side logic for managing supervisors
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create:function(req,res){
		var id ;
		function set(newId){
			id = newId;
		}
		var newperson = req.allParams();
		//newperson.photo = files[0].fd;
		//console.log(newperson);
		Person.create(newperson).exec(function cb(err,person){
			if(err){
				res.json({error:err,success:false});
			}else{
				var data = req.allParams();
				data.person = person.id;
				if(data.password.length > 20){
					res.json({
						err:"longitud maxima de 20 caracteres",success:false
					});
				}else{
					data.password=bcrypt.hashSync(data.password);
					Operator.create(data).exec(function cb(err2,obj2){
						if(err2){
							res.json({err:err2,success:false});
						}else{
							res.json({err:null,success:true,person:person,user:obj2});
						}
					});					
				}
			}
		});
	},		
	login:function(req,res){
		if(req.param('email_login') && req.param('passwd') ){
			var mail = req.param('email_login');
			var pass = req.param('passwd');
			Supervisor.findOne({email_login:mail}).exec(function(err,user){
				if(err){
					res.json({err:err,success:false});
				}else{
					if(user && bcrypt.compareSync(pass,user.password)){
						Person.findOne({id:user.person}).exec(function(errp,person){
							if(errp){
								res.json({err:errp,success:false});
							}else if(person){
								person.userId=user.id
								person.email_login=user.email_login;
								person.photo = person.photo;
								person.name = person.first_name;
								res.cookie('signin','true',{signed:true});
								res.json({err:null,success:true,person:person});
							}else{
								res.json({err:null,success:true,person:null});
							}
						});
					}else{
						res.json({err:'Usuario o constraseña invalida',success:false});
					}
				}
			});
		}else{
			res.json({err:'Parametros invalidos',success:false});
		}
	}	
};

