/**
 * TmpuserController
 *
 * @description :: Server-side logic for managing tmpusers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	_config:{
		actions:false,
		shortcuts:false,
		rest:false
	},		
	registerUser:function(req,res){
		if(req.param('mail')){
			var code = genCode();
			var mail = req.param('mail');
			//validando si el usuario existe
			Users.count({email_login:mail}).exec(function cb(err,n){
				if(err){
					res.json({err:err,success:false});
				}
				if(n > 0 ){
					res.json({err:'El correo ya se encuentra registrado',success:false});
				}else{
					//enviando mail
					NodeMailer(mail,code,function(nmerr,nmres){
						if(nmerr){
							res.json({err:nmerr,success:false});
						}else if(nmres){
							console.log('in')
							Tmpuser.create({email_login:mail,tmp_code:code}).
								exec(function cb(tmperr,tmpobj ){
									if(tmperr){
										res.json({err:tmperr,success:false});
									}else if(tmpobj){
										res.json({err:null,success:true});
									}
								});
						}else{
							res.send('error')
						}
					});
				}
			});
			function genCode(){
				return Math.random().toString().substring(2,8);
			};			
		}else{
			res.json({
				err:'invalid params',
				success:false
			})
		}

	},
	validateUser:function(req,res){
		var mail = req.param('mail');
		var code = req.param('code');

		Tmpuser.count({email_login:mail,tmp_code:code}).exec(function(err,n){
			if(err){
				res.send({err:err,success:false});
			}
			if(n > 0){
				res.send({err:null,success:true});
			}else{
				res.send({err:"no estas registrado",success:false});
			}
		});

	}
};

