/**
 * OperatorController
 *
 * @description :: Server-side logic for managing operators
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var bcrypt = require('bcrypt-nodejs');
var path = require('path');
var chance = require('chance');

var random = new chance() ;

module.exports = {
	_config:{
		actions:false,
		shortcuts:false,
		rest:false
	},	
	resetPasswd:function(req,res){
		var operator = req.param('operator');
		Operator.findOne({id:operator.id})
			.exec(function(error,result){
				if(error){
					res.json({error:error,success:false});
				}else if(result){
					var str = random.string({length:6});
					Operator.update({id:operator.id},{password : str })
							.exec(function(updateError,updated){
								if(updateError){
									res.json({error:updateError,success:false});
								}else{
									updated[0].passwdToken = str ; 
									OperatorMailer(updated[0],function(nmerr,nmres){
										if(nmerr){
											res.json({err:nmerr,success:false});
										}else if(nmres){
											res.json({err:null,success:true,operator:result});
										}else{
											res.json({error:'error sending email',success:false})
										}
									});
								}
							})
				}else{
					res.json({error:'no operator found',success:false})
				}
			})
	},
	getAll:function(req,res){
		Operator.find()
				.exec(function(error,operators){
					res.json({errors:error,operators:operators})
				});
	},
	getOperator:function(req,res){
		Operator.find({displayName:req.param('displayName')})
				.exec(function(error,operator){
					res.json({error:error,operators:operator});
				});
	},
	updateProfile:function(req,res){
		Operator.update({id:req.param('agent').id},req.param('agent'))
				.exec(function(error,updated){
						if(error){
							res.json({err:error,success:false});
						}else{
							res.json({err:null,success:true,updated:updated});
						}
				})
	},
	create:function(req,res){
		var data = req.param('agent');
		data.password=bcrypt.hashSync('123456');
		Operator.create(data).exec(function cb(err2,obj2){
			if(err2){
				res.json({err:err2,success:false});
			}else{
				//res.json({err:null,success:true});
				OperatorMailer(data,function(nmerr,nmres){
					if(nmerr){
						res.json({err:nmerr,success:false});
					}else if(nmres){
						console.log('in')
						res.json({err:null,success:true,operator:obj2});
					}else{
						res.send('error sending email')
					}
				});
			}
		});							
	},	
	login:function(req,res){
		console.log(req.allParams());
		if(req.param('email_login') && req.param('passwd') ){
			var mail = req.param('email_login');
			var pass = req.param('passwd');
			Operator.findOne({email:mail}).exec(function(err,user){
				if(err){
					console.log(err)
					res.json({err:err,success:false});
				}else{
					if(user && bcrypt.compareSync(pass,user.password)){
						res.json({err:null,success:true})
					}else{
						res.json({err:'Usuario o constraseña invalida',success:false});
					}
				}
			});
		}else{
			res.json({err:'Parametros invalidos',success:false});
		}
	},
	home:function(req,res){
		//if(req.signedCookies.signin){
			res.view('operator_home')
		/*}else{
			res.view('login')
		}	*/
	},
	historial:function(req,res){
		res.view('operator_historial');
	}
};
