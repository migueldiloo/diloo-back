/**
 * ChatController
 *
 * @description :: Server-side logic for managing chats
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	push:function(req,res){
		FireBase.push(req,res);
	},
	pull:function(req,res){
		FireBase.pull(req,res);
	}
};
