/**
 * MessageController
 *
 * @description :: Server-side logic for managing Messages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Kaiseki = require('kaiseki');
// instantiate
var APP_ID = 'J6I4F5i0OuvuwPK4JA2nagvKkKGhaTxnx4sfjePE';
var REST_API_KEY = '1fobjhZfawizPVbPLnMsyXudDAbqDcUmBhFtxoGO';
var kaiseki = new Kaiseki(APP_ID, REST_API_KEY);

module.exports = {
	_config:{
		actions:false,
		shortcuts:false,
		rest:false
	},	
	join:function(req,res){
		var room =  ('ticket:'+  req.param('room') );
		sails.sockets.join(req.socket,room);
		res.json({err:null,success:true,msg:('suscrito al ticket:'+ room)});
	},
	//from movil
	postMessage:function(req,res){
		var type=req.param('type');
		var body=req.param('body');
		var ticket = req.param('ticket'); 
		var author= req.param('author');
		//var data = {type:type,body:body,ticket:ticket,author:author}
		//console.log(req.allParams());

		Message.create(req.allParams()).exec(function cb(err,msg){
			if(err){
				res.json({err:err,success:true});
			}else if(msg){
				var room = ('ticket:'+ticket);
				//console.log(room);
				sails.sockets.broadcast( room,'msg',{body:body,type:'sender',ticket:ticket,author:author});
				res.json({err:null,success:true});
			}
		});
	},
	//from web
	sendMessage:function(req,res){	
		if(req.param('ticket')){
			var type=req.param('type');
			var body=req.param('body');
			var ticket = req.param('ticket'); 
			var author = req.param('author');
			var company = req.param('company')
			var data = {type:type,body:body,ticket:ticket,author:author,company:company};
			//console.log(data);
			Ticket.findOne({id:ticket}).exec(function(terr,ticketObj){
				if(ticketObj){
					var info = {
						"where":{
							"channels":'',
							"uid":ticketObj.user
						},
						"data":{
							"alert":body,
							"title":company.name,
							"company":company,
							"ticket":ticketObj.id,
							"date":new Date().toJSON()
						}
					};
					/*console.log(ticketObj);*/
					//console.log(info);
					Message.create(data).exec(function cb(err,msg){
						if(err){
							res.json({err:err,success:true});
						}else if(msg){
							kaiseki.sendPushNotification(info,function(kerr,kres,kbody,ksuccess){
							});
							Ticket.update({id:ticket},{status:1,operator:1}).exec(function(uerr,updated){
								if(uerr){
									res.json({err:uerr,success:false});
								}else{
									res.json({err:null,success:true});
								}
							});						
						}
					});	
				}else{
					res.json({err:terr,success:false});
				}
			});
		}
	},
	//gmft get messages from tickets..
	gmft:function(req,res){
		var ticket = req.param('ticket');
		Message.find({ticket:ticket}).exec(function(err,tickets){
			if(err){
				res.json({err:err,tickets:null});
			}else{
				res.json({err:null,tickets:tickets});
			}
		});
	}
};

