/**
 * TicketController
 *
 * @description :: Server-side logic for managing tickets
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Kaiseki = require('kaiseki');
// instantiate
var APP_ID = 'J6I4F5i0OuvuwPK4JA2nagvKkKGhaTxnx4sfjePE';
var REST_API_KEY = '1fobjhZfawizPVbPLnMsyXudDAbqDcUmBhFtxoGO';
var kaiseki = new Kaiseki(APP_ID, REST_API_KEY);


module.exports = {
	_config:{
		actions:false,
		shortcuts:false,
		rest:false
	},
	review:function(req,res){
		//user & company  are required both be id´s
		var data ={};
		data.status = 0;
		data.area = 1;
		data.user = parseInt(req.param('user'));
		data.company= parseInt(req.param('company'));
		//console.log(req.allParams());
		Ticket.findOne({user:data.user,company:data.company,status:{'<':2},area:data.area}).
				exec(function(ferr,fticket){
						if(ferr){
							res.json({err:ferr,success:true,ticket:null});
						}else if(fticket){
							res.json({err:null,success:true,url: fticket.id });
						}else{
							Ticket.create(data).
									exec(function(cerr,cticket){
										if(cerr){
											res.json({err:cerr,success:true,ticket:null});
										}else{
											//enviando el ticket a su room
											var room = (req.param('company') +':'+ data.area) ;
											sails.sockets.broadcast(room,'newticket',{ticket:cticket});
											res.json({err:null,success:true,url: cticket.id });
										}
									})	
						}
				});
		//console.log(req.allParams());
	},
	join:function(req,res){
		var room = req.param('room');
		sails.sockets.join(req.socket,room);
		res.json({err:null,success:true,msg:('suscrito  a: '+ room)});
	},	
	getMyTickets:function(req,res){
		//si lo pide el usuario
		if(req.param('movil') && (req.param('movil').toString() === 'active') ){
			Ticket.find({user:req.param('userId'),status:1}).exec(function(err,tickets){
				if(err){
					res.json({err:err,success:false});
				}else if(tickets){
					res.json({err:null,success:true,tickets:tickets});
				}else{
					res.json({err:null,success:true,tickets:null});
				}
			});	
		//si lo pide el operador --tickets abiertos
		}else if( req.param('web') &&  ( req.param('web').toString() === 'true') ){
			Ticket.find({company:req.param('companyId'),
						operator:req.param('operatorId'),
						area:req.param('areaId'),
						status:1})
					.exec(function(err,tickets){
						if(err){
							res.json({err:err,success:false});
						}else if(tickets){
							//console.log(tickets)
							res.json({err:null,success:true,tickets:tickets});
						}else{
							res.json({err:null,success:true,tickets:null});
						}
					}
			);	
		}

	},
	//tickets del area --pendientes
	getAreaTickets:function(req,res) {
		Ticket.find({company:req.param('companyId'),area:req.param('areaId'),status:0})
				.exec(function(err,tickets){
					if(err){
						res.json({err:err,tickets:null,success:false});
					}else if(tickets){
						res.json({err:null,tickets:tickets,success:true});
					}else{
						res.json({err:null,tickets:null,success:true});
					}
				});
	},
	closeTicket:function(req,res){
		Ticket.update({id:req.param('ticketId')},{status:2}).exec(function(err,updated){
			if(err){
				res.json({err:err,success:false});
			}else if(updated){
				sails.sockets.leave(req.socket,'ticket:'+req.param('ticketId'));
				console.log(JSON.stringify(sails.sockets.socketRooms(req.socket)))
				//console.log(updated);
				var info = {
					"where":{
						"channels":'',
						"uid":updated[0].user
					},
					"data":{
						"alert":'cerrado',
						"title":'cerrado',
						"company":updated[0].company,
						"ticket":updated[0].id,
						"date":new Date().toJSON()
					}
				};		
				//console.log('---');
				//console.log(info)	;
				kaiseki.sendPushNotification(info,function(kerr,kres,kbody,ksuccess){
				});					
				res.json({err:null,success:true});
			}else{
				res.json({err:null,success:true,ticket:null});
			}			
		});
	},
	getClosedTicketsInArea:function(req,res){
		if( req.param('areaId') && req.param('companyId') ){
			var skip = req.param('skip') || 0;
			if(!isNaN(skip) && skip <= 0) skip = 0; //validando el paginador
			var query ='select * from getCloseds  where  companyid = '+req.param('companyId')+ ' and areaid = ' +req.param('companyId')+' limit '+skip+',25';
			//console.log(query);
			Ticket.query(query,function(err,tickets){
				if(err) res.json({err:err,success:false});
				var rLen = tickets.length;
				res.json({tickets:tickets,len:rLen,success:true});
				//res.json({err:err,tickets:tickets});
			})
		}else{
			res.json({err:'invalid userId',success:false});
		}
	},
	getMessagesFromTicket:function(req,res){
		if(req.param('ticketId')){
			Message.find({ticket:req.param('ticketId')})
					.exec(function(err,messages){
						if(err) res.json({err:err,success:false});
						res.json({messages:messages,success:true});
					});
		}else{
			res.json({err:'invalid params',success:false});
		}
	},
	getClosedTicketsInAreaByUserName:function(req,res){
		if( req.param('areaId') && req.param('companyId') && req.param('userName')){
			var skip = req.param('skip') || 0;
			if(!isNaN(skip) && skip <= 0) skip = 0; //validando el paginador
			var query ='select * from getCloseds  where  companyid = '+req.param('companyId')+ ' and areaid = ' +req.param('companyId')+' and user like "%'+req.param('userName')+'%" limit '+skip+',25';
			//console.log(query);
			Ticket.query(query,function(err,tickets){
				if(err) res.json({err:err,success:false});
				var rLen = tickets.length;
				res.json({tickets:tickets,len:rLen,success:true});
				//res.json({err:err,tickets:tickets});
			})
		}else{
			res.json({err:'invalid userId',success:false});
		}
	},	

};

