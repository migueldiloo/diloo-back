window.counter = 0;
window.dilooApp = {
  operatorId:1,
  companyId : 1,
  areaId:1,
  tickets : [],
  companyData:{
    "areas": [],
    "supervisors": [],
    "operators": [],
    "category": {
      "id": 1,
      "name": "Seguros",
      "status": true,
      "createdAt": "2015-05-28T19:36:51.000Z",
      "updatedAt": "2015-05-28T19:36:51.000Z"
    },
    "id": 1,
    "name": "Banbif",
    "image": "http://1.bp.blogspot.com/-AVVV9V9UqkU/UZuywn64u8I/AAAAAAAAAD8/YOhnQj9cGXA/s1600/imagen falabella.jpg",
    "bgImage": "http://1.bp.blogspot.com/-AVVV9V9UqkU/UZuywn64u8I/AAAAAAAAAD8/YOhnQj9cGXA/s1600/imagen falabella.jpg",
    "description": "banbif",
    "id": 1,
    "name": "Banbif",
    "image": "/companyLogos/banbif.jpg",
    "bgImage": "/companyLogos/banbif.jpg",
    "description": "Entidad aseguradora",
    "status": true,
    "createdAt": "2015-05-28T19:43:16.000Z",
    "updatedAt": "2015-05-28T19:43:16.000Z"
  },
  clearBox:function(key){//ok
    $('form[data-ticket="'+key+'"] textarea').val('');
  },
  renderPendientes:function(obj){//ok
    var message = obj.messages[obj.messages.length - 1]  || 'Nuevo mensaje';
    //console.log(obj);
    $('.in-comming ul').append('<li>'+
	    							  '<a  data-key="'+obj.id+ '">'+
	    								   '<img src="'+ obj.userInfo.photo+'" alt="'+obj.userInfo.nombres+'" title="'+obj.userInfo.nombres+'">'+
                         '<div class="counter"><p>'+obj.messages.length+'</p></div>'+
	      							'</a>'+
      							'</li>')
  },
  renderChat:function(key,other){//ok
      var key = key;
      var chats = [];
      var userInfo = {} ;
      for(var i = 0;i<window.dilooApp.tickets.length;i++){
        if(window.dilooApp.tickets[i].id == key ){
          chats=window.dilooApp.tickets[i].messages;
        }
      }
      for(var i = 0 ; i < window.dilooApp.tickets;i++){
        if(window.dilooApp.tickets[i].id=key){
          userInfo = window.dilooApp.tickets[i].id;
          return
        }
      }
      console.log(chats)
      var dom = "";
      for(var i = 0 ; i< chats.length;i++){
        if( (chats[i].type).toLowerCase() =='sender' ){
          dom+="<li class='nuevoSender'>"+
                  "<div class='user-photo'>"+
                    "<img src='"+userInfo.photo+"'/>"+
                    "<div class='requiem'>"+
                      "<div class='triangle'></div>"+
                      "<p>"+chats[i].body+"</p>"+
                    "</div>"
                  "</div>"+
                "</li>";
          return;
        }else{
          dom+="<li class='reciber'><div class='triangle'></div><p>"+chats[i].body+"</p></li>"
        }      
      }
      if(typeof other === 'object'){
         return dom;
      }else {
        $("ul[data-key='"+key+"']").html(dom)   
      }
  },
  renderChatWindow:function(key,chatList){
      var userInfo ;
      var messages ;
      var str="";
      for(var i=0;i<window.dilooApp.tickets.length;i++){
        var token= window.dilooApp.tickets[i];
        if(token.id == key){
          userInfo = token.userInfo;
          messages = token.messages;
        }
      }
      for(var i = 0 ; i< messages.length;i++){
        if( (messages[i].type).toLowerCase() =='sender' ){
          str+="<li class='nuevoSender'>"+
                  "<div class='user-photo'>"+
                    "<img src='"+userInfo.photo+"'/>"+
                    "<div class='requiem'>"+
                      "<div class='triangle'></div>"+
                      "<p>"+messages[i].body+"</p>"+
                    "</div>"+
                  "</div>"+
                "</li>";

        }else{
          str+="<li class='reciber'><div class='triangle'></div><p>"+messages[i].body+"</p></li>"
        }      
      }
      //console.log(userInfo)
      $(".elements").append('<div class="chat-open" data-ticket="'+key+'">'+
                  							'<header>'+
                  								//'<div class="user-photo">'+
                  									//'<img src="'+userInfo.photo+'">'+
                  								//'</div>'+
                  								'<div class="user-name">'+
                  									'<p>'+userInfo.nombres+'</p>'+
                                    '<img src="/img/+.png" class="icono"/>'+
                  								'</div>'+
                                  '<div class="dropdown" style="display:none">'+
                                    '<p>'+
                                      '<img src="/img/telf.png"/><span>'+userInfo.phone+'</span>'+
                                    '</p>'+
                                    '<p>'+
                                      '<img src="/img/email.png"/><span>'+userInfo.email+'</span>'+
                                    '</p>'+
                                  '</div>'+
                  								'<div class="window-actions">'+
                  									'<a class="min" href="#">'+
                                      '<img src="/img/_.png" width="10px">'+
                                    '</a>'+
                  									'<a class="close" href="#" data-key="'+key+'">'+
                                      '<img src="/img/x.png" width="10px">'+
                                    '</a>'+
                  								'</div>'+
                                  /*'<section>'+
                                    '<p><span></span><span>'+userInfo.phone+'</span></p>'+
                                    '<p><span></span><span>'+userInfo.email+'</span></p>'+
                                  '</section>'+*/
                              	'</header>'+
                              	'<div class="messages">'+
                              		'<ul data-key="'+key+'">'+
                              			str+
                              		'</ul>'+
                              	'</div>'+
                              	'<div class="send">'+
                              		'<form method="POST" action="" data-ticket="'+key+'">'+
                              			'<textarea  name="message" placeholder="Escribe una respuesta" required></textarea>'+
                              			'<fieldset>'+
                              				'<input type="file",name="file">'+
                              				'<span class="icon-paper-clip"></span>'+
                              				'<p>Pulsa "Enter" para enviar.</p>'+
                              			'</fieldset>'+
                              		'</form>'+
                              	'</div>'+
                            '</div>'
                            );
      $(".minificados").append('<div class="chat-min" data-ticket="'+key+'">'+
                              '<a href="#" data-key="'+key+'">'+
                                '<img alt="'+userInfo.nombres+'" title="'+userInfo.nombres+'" src="'+userInfo.photo+'"/>'+
                                /*'<div class="counter">'+
                                  '<p>'+messages.length+'</p>'+
                                '</div>'+*/
                              '</a>'+
                            '</div>');
    setTimeout(function(){
      $(".chat-open[data-ticket='"+key+"'] .messages").scrollTop($(".chat-open[data-ticket='"+key+"'] .messages ul").height());
      $(".chat-open[data-ticket='"+key+"'] textarea").focus();
    },500);

    setHeight();
  }  

}
var room= window.dilooApp.companyId+':'+window.dilooApp.areaId;
//obtengo mis tickets abiertos
io.socket.get('/ticket/getMyTickets',
              {companyId:window.dilooApp.companyId,
                operatorId:window.dilooApp.operatorId,
                areaId:window.dilooApp.areaId,
                web:'true'
              },
              function(data){
                console.log('obteniendo tickets');
                var tokens=data.tickets;
                var length = tokens.length;
                var counter = 0;
                //for(var i = 0;i<tokens.length;i++){
                  function getUserInfo(){
                      if(counter == length) return ;
                      var item = tokens[counter];
                      item.messages=[];
                      io.socket.get('/user/getUserInfo',{userId:item.user},function(data){
                                    //var chatList=window.dilooApp.renderChat(tokens[i].id,null); //dom elements
                                    item.userInfo=data.user;
                                    window.dilooApp.renderChatWindow(item.id,'')
                                    io.socket.get('/message/gmft',
                                              {
                                                ticket:item.id
                                              },
                                              function(msgs){
                                                   var len = msgs.tickets.length;
                                                   for(var k=0;k<len;k++){
                                                      var ntoken = msgs.tickets[k];
                                                      item.messages.push(ntoken);
                                                      if( (ntoken.type).toLowerCase() =='sender' ){
                                                        $('ul[data-key="'+item.id+'"]').append("<li class='nuevoSender'>"+
                                                                "<div class='user-photo'>"+
                                                                    "<img src='"+item.userInfo.photo+"'/>"+
                                                                    "<div class='requiem'>"+
                                                                      "<div class='triangle'></div>"+
                                                                      "<p>"+ntoken.body+"</p>"+
                                                                    "</div>"+
                                                                "</div>"+
                                                              "</li>")

                                                      }else{
                                                        $('ul[data-key="'+item.id+'"]').append("<li class='"+ntoken.type+"'><div class='triangle'></div><p>"+ntoken.body+"</p></li>");
                                                      } 
                                                   }
                                              }
                                    );
                                    console.log(item.id)
                                    io.socket.get('/message/join',
                                                  {
                                                    room:item.id
                                                  },
                                                  function(m2){
                                                    console.log(m2.msg);
                                                  }
                                    );                                    
                                  });
                      window.dilooApp.tickets.push(item);
                      counter++;
                      getUserInfo();
                  }   
                  getUserInfo();
                //}         
              }
);
//obtengo mis tickets pendientes
io.socket.get('/ticket/getAreaTickets',
              {companyId:window.dilooApp.companyId,
                areaId:window.dilooApp.areaId
              },
              function(data){
                console.log('listando pendientes del área')
                var tokens=data.tickets;
                //console.log(tokens);
                //for(var i = 0;i<tokens.length;i++){
                  var length = tokens.length;
                  var counter = 0;
                    //consultando la info del usuario
                    function getUserInfo(){
                      if(counter == length) return;
                      var self = tokens[counter];
                      self.messages=[];
                      io.socket.get('/user/getUserInfo',{userId:self.user},function(data){
                                    self.userInfo=data.user;
                                    //uniendolo al room del ticket para que reciba notificaciones
                                    io.socket.get('/message/join',
                                                  {
                                                    room:self.id
                                                  },
                                                  function(m2){
                                                    console.log(m2.msg);
                                                  }
                                    );
                                    io.socket.get('/message/gmft',
                                                {
                                                  ticket:self.id
                                                }
                                                ,function(messages){
                                                  //console.log(messages)
                                                  self.messages=messages.tickets;
                                                  //agregando la data a pendientes
                                                  console.log(self);
                                                  window.dilooApp.tickets.push(self);
                                                  window.dilooApp.renderPendientes(self);  
                                                }
                                    );                               
                                  });
                      counter++;
                      getUserInfo();
                    }
                                  //obtengo los mensajes de los tickets pendientes
                                  /*io.socket.get('/message/gmft',
                                                {
                                                  ticket:self.id
                                                }
                                                ,function(messages){
                                                  //console.log(messages)
                                                  self.messages=messages.tickets;
                                                  //agregando la data a pendientes
                                                  window.dilooApp.tickets.push(self);
                                                  window.dilooApp.renderPendientes(self);   
                                                }
                                  );  */             
                                //}
                   // )

                //}
                getUserInfo();
              }
);
//uniendolo a un room
io.socket.get('/ticket/join',
              {
                room:room
              },
              function(data){
                console.log('uniendose al área ' + room)
                console.log(data.msg);
              }
);
//recibiendo tickets nuevos
io.socket.on('newticket',function(data){
              var ticket = data.ticket;
              ticket.messages=[];
              //console.log(data)
              io.socket.get('/user/getUserInfo',
                          {
                            userId:ticket.user
                        },
                        function(userInfo){
                          ticket.userInfo=userInfo.user;
                          //agregando la data a pendientes
                          window.dilooApp.tickets.push(ticket);
                          window.dilooApp.renderPendientes(ticket);
                          //uniendolo al room del ticket para que reciba notificaciones
                          io.socket.get('/message/join',
                                        {
                                          room:ticket.id
                                        },
                                        function(m2){
                                          console.log(m2);
                                        }
                          );                                  
                        }
              )

});
//nuevo mensaje
io.socket.on('msg',function(data){
  console.log('nuevo mensaje');
  console.log(data);
  var u ;
  //$("[data-key="+data.ticket+"] .chat-info p:eq(1)").text(data.msg);
  var len = window.dilooApp.tickets.length;
  for(var i =0;i<len;i++){
    var token = window.dilooApp.tickets[i];
    if(token.id == data.ticket){
      window.dilooApp.tickets[i].messages.push(data);
      var userInfo= window.dilooApp.tickets[i].userInfo;
      $('ul[data-key="'+data.ticket+'"]').append(

              "<li class='nuevoSender'>"+
                  "<div class='user-photo'>"+
                    "<img src='"+userInfo.photo+"'/>"+
                    "<div class='requiem'>"+
                      "<div class='triangle'></div>"+
                      "<p>"+data.body+"</p>"+
                  "</div>"+
                "</li>"

        );
      u = window.dilooApp.tickets[i];
    }
  }
  $("a[data-key='"+data.ticket+"'] .counter p").text(u.messages.length);
  var dom =$(".chat-open[data-ticket='"+data.ticket+"']");
  $(".chat-min[data-ticket='"+data.ticket+"']").addClass('animated bounce');
  $(".chat-min[data-ticket='"+data.ticket+"']").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
    $(".chat-min[data-ticket='"+data.ticket+"']").removeClass('animated bounce');
  });
  dom.scrollTop(dom.height()); 

  setTimeout(function(){
    $(".chat-open[data-ticket='"+data.ticket+"'] .messages").scrollTop($(".chat-open[data-ticket='"+data.ticket+"'] .messages ul").height());
  },500);

});
//-------------------metodos del dom
//mostrar ventana de chat
$('.in-comming ul').delegate("a",'click',function(e){
  e.preventDefault();
  if(window.counter < 3){
    var key = $(this).attr('data-key');
    var chatList=window.dilooApp.renderChat(key,null); //dom elements
    window.dilooApp.renderChatWindow(key,chatList);
    $(this).parent().remove();
    window.counter ++;
  }

});
//respondermensajes
$("body").delegate("textarea","keydown",function(e){
  if( e.which == 13){
  e.preventDefault();    
    var val = $(this).val();
    var url = $(this).parent().attr('data-ticket');
    var message = {
      author:'OPERATOR',
      body:val,
      type:'reciber',
      ticket:url,
      company:window.dilooApp.companyData
    };
    console.log(message)
    //pushing
    io.socket.get('/message/web',
                  message,
                  function(data){
                    console.log(data);
                    /*if(data.ksuccess ==true){
                      var len =window.dilooApp.tickets.length;
                      for(var i =0;i<len;i++){
                        var token=window.dilooApp.tickets[i];
                        if(token.id==url){
                          token.messages.push(message);*/
                          $('ul[data-key="'+url+'"]').append("<li class='"+message.type+"'><div class='triangle'></div><p>"+message.body+"</p></li>");
                       /* }
                      }
                    }*/
                  }
    );
    window.dilooApp.clearBox(url);

    setTimeout(function(){
      $(".chat-open[data-ticket='"+url+"'] .messages").scrollTop($(".chat-open[data-ticket='"+url+"'] .messages ul").height());
    },500);
  }
});

//finalizando chat
$("body").delegate('.close','click',function(e){
    e.preventDefault();
    var key = $(this).attr('data-key');
    /*diloo.child(key).update({
      'responsed':'closed'
    });
    $(this).parent().parent().fadeOut();*/
    $("[data-component='close-chat']").attr('data-key',key);
    $("[data-component='close-chat']").fadeIn();

});
$('[data-component="close-chat"] .exit').on('click',function(){
  $('[data-component="close-chat"]').fadeOut();
});
$('[data-component="close-chat"] .end').on('click',function(){
    var key = $('[data-component="close-chat"]').attr('data-key');

    io.socket.get('/ticket/closeTicket',
                  {
                    ticketId:key
                  },
                  function(data){
                    console.log(data)
                  }
    );
    $(".chat-open[data-ticket='"+key+"']").fadeOut();
    $('[data-component="close-chat"]').fadeOut();
    //$('[data-component="chat-window"] '
    $(".chat-open[data-ticket='"+key+"']").remove();
    $(".chat-min[data-ticket='"+key+"']").remove();
    $("a[data-key='"+key+"']").parent().remove();
    window.counter-- ;
}); 



$(document).on('ready',init);
function init(){
	setWidth();
	setHeight();
	$(window).resize(function(){setHeight();setWidth();});

	$('body').delegate('.min', 'click',function(e){
    //alert('s')
		e.preventDefault();
		var self = $(this).parent().parent().parent();
		var ticket =self.attr('data-ticket');
		$(self).css({
			'display':'none'
		});
		$('.chat-min[data-ticket="'+ticket+'"]').fadeIn('fast');
	});

	$('body').delegate('.chat-min', 'click',function(e){
		e.preventDefault();
		var ticket = $(this).attr('data-ticket');
		$(this).fadeOut('fast');
		$('.chat-open[data-ticket="'+ticket+'"]').fadeIn('fast')
	});

	$(".select").on('click',function(){

		$(this).children('.wrapper').slideToggle();
	});


  $("#login form").on('submit',function(e){
    e.preventDefault();
    $.ajax({
      url:$(this).attr('action'),
      method:$(this).attr('method'),
      data:$(this).serialize(),
      error:function(data){
        alert(data);
      },
      success:function(data){
        if(data.success){
          alert('bienvenido :)');
          window.location.href='/operator/home'
        }else{
          alert(data.err)
        }
      }
    })
  })

}

function setWidth(){
	var w = window.innerWidth;
	$('#operator-home .open').css({
		'width':(w-290)+'px'
	});
}
function setHeight(){
	var h = window.innerHeight;
	$('#operator-home .in-comming ul').css({
		'height':(h-76)+'px'
	});
  $(".chat-open").css({
    'height':(h-30 -76 )+'px'
  });
  $("#operator-home .open > .elements .chat-open").css({
    //'margin-top': '-'+(h-103)+'px'
    'margin-top':'0px'
  });
  $("#operator-home .open > .elements .chat-open .messages").css({
    'height':(h-280)+'px'
  });
  /*$(".chat-min").css({
    'margin-top':(h-300)+'px'
  })*/
}
$("body").delegate('.user-name','click',function(){
  var icon = $(this).children('img').attr('src');
  if(icon=='/img/+.png'){
    $(this).children('img').attr('src','/img/_.png').attr('width','15px');;
  }else{
    $(this).children('img').attr('src','/img/+.png').attr('width','');
  }
  $(this).siblings('.dropdown').slideToggle('fast');
})
setTimeout(function(){
  setHeight();
},500)