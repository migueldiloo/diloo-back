angular.module('diloo',['ngSails'])
		.config(['$sailsProvider', function ($sailsProvider) {
    		//$sailsProvider.url = 'http://localhost:1337/';
		}])
		.factory('Excel',function($window){
	        var uri='data:application/vnd.ms-excel;base64,',
	            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
	            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
	            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
	        return {
	            tableToExcel:function(tableId,worksheetName){
	                var table=document.getElementById(tableId),
	                    ctx={worksheet:worksheetName,table:table.innerHTML},
	                    href=uri+base64(format(template,ctx));
	                return href;
	            }
	        };
	    })		
		.controller('historyController',historyController)
		.controller('homeController',homeController)
		.controller('agentController',agentController);

function agentController($scope,$sails){
	$scope.title = 'Administrar agentes';
	$scope.agent={
		data:{},
		show:{
			create:false,//create 
			update:false,
			list:true,
			windowEdit:function(agent){
				$scope.agent.data=agent;
				$scope.agent.show.create=false;
				$scope.agent.show.list = false;
				$scope.agent.show.update = true;
				$scope.title="Editar agente"


				console.log($scope.agent.data)
			},
			windowCreate:function(){
				$scope.agent.data={};
				$scope.agent.data.status = true;
				$scope.agent.show.create=true;
				$scope.agent.show.list = false;
				$scope.agent.show.update = false;
				$scope.title="Agregar agente"
			},
			windowList:function(){
				$scope.agent.show.create=false;
				$scope.agent.show.list=true;
				$scope.agent.show.update = false;
				$scope.title = 'Administrar agentes';
			}
		},
		del : function(agent){
			console.log(agent)
			console.log('del agent')
		},
		create : function (agent){
			console.log('create');
			agent.company=1;
			$sails.post('/operator/create',{agent})
					.success(function(data,status,headers,jwr){
						if(data.success){
							alert('agente creado');
							$scope.getAll();
							$scope.agent.data={};
							$scope.agent.show.windowList();
						}else{
							alert('ha ocurrido un error')
						}
					})
					.error(function(data,status,headers,jwr){
						alert('ha ocurrido un error')
					})			
		},
		update:function(agent){
			console.log(agent)
			console.log('update');
			$sails.post('/operator/update',{agent})
					.success(function(data,status,headers,jwr){
						if(data.success){
							alert('agente modificado');
							$scope.getAll();
							$scope.agent.data={};
							$scope.agent.show.windowList();
						}
					})
					.error(function(data,status,headers,jwr){
						alert('ha ocurrido un error')
					})				
		},
		all:[] //all agents goes here
	}
	//get all operators
	$scope.getAll=function(){
		$sails.get('/operator/getAll')
			.success(function(data,status,headers,jwr){
				if(data.operators.length){
					//$scope.title = 'Hay ' + data.operators.length + ' operadores';
					$scope.agent.all = data.operators
				}else{
					$scope.title = 'No hay operadores'
				}
			})
			.error(function(data,status,headers,jwr){
				console.log(status)
				console.log(data)
			})
	};
	$scope.getOperator=function(displayName){
		$sails.post('/operator/getOperator',{displayName:displayName})
			.success(function(data,status,headers,jwr){
				console.log(status)
				console.log(data)				
				if(data.operators.length){
					$scope.agent.all = data.operators
				}
			})
			.error(function(data,status,headers,jwr){
				console.log(status)
				console.log(data)
			})
	};
	$scope.reset_passwd=function(operator){
		$sails.post('/operator/resetPasswd',{operator:operator})
			.success(function(data,status,headers,jwr){
				if(data.success){
					alert('clave cambiada');
				}else{
					alert('ha ocurrido un problema');
				}
			})
			.error(function(data,status,headers,jwr){
				console.log(status)
				console.log(data)
			})		
	}
	//exec
	$scope.getAll();
}
function historyController($scope,$sails,Excel,$timeout){
	$scope.tickets={
		data: [],
		total:0
	}
	$scope.ticketStatus = false;
	$scope.user = {
		data:{},
		show:false
	};
	$scope.messages={
		data:[],
		show:true
	};
	$scope.jump = 0 ;
	$scope.loadMessages=function(){
			$sails.get("/ticket/getCloseds",{areaId:1,companyId:1,skip:$scope.jump})
			.success(function(data,status,headers,jwr){
				$scope.tickets.data = data.tickets;
				$scope.tickets.total= data.len;
				console.log(data)
			})
			.error(function(data,status,headers,jwr){
				console.log(status)
			});
	}
	$scope.loadMessages();//lauching
	$scope.getMessages=function(ticket){
		//getting user info
		$sails.get("/user/getUserInfo",{userId:ticket.userid})
				.success(function(data,status,headers,jwr){
					//console.log(data);
					$scope.user.data=data.user;
				})
				.error(function(data,status,headers,jwr){
					console.log(status)
				});
		//getting messages
		$sails.get("/ticket/gmft",{ticketId:ticket.ticketid})
				.success(function(data,status,headers,jwr){
					console.log(data);
					$scope.messages.data=data.messages;
				})
				.error(function(data,status,headers,jwr){
					console.log(status)
				});		
		//changing ticket state
		$scope.ticketStatus=true;
	}
	$scope.findByUser=function(userName){
		$sails.get("/ticket/findByUser",{areaId:1,companyId:1,skip:$scope.jump,userName:userName})
			.success(function(data,status,headers,jwr){
				$scope.tickets.data = data.tickets;
				$scope.tickets.total= data.len;
				console.log(data)
			})
			.error(function(data,status,headers,jwr){
				console.log(status)
			});
	}
	$scope.closeOpenTicket=function(){
		$scope.ticketStatus=!$scope.ticketStatus;
	}
	$scope.showUser=function(){
		$scope.user.show = true;
		$scope.messages.show = false
	}
	$scope.showMessages=function(){
		$scope.messages.show = true;
		$scope.user.show = false;
	}
	$scope.paginationNext=function(){
		$scope.jump >= $scope.tickets.total ? $scope.jump+=25:$scope.jump+=0;
		$scope.loadMessages();
		console.log($scope.jump)
	}
	$scope.paginationPrev=function(){
		$scope.jump <= 0 ? $scope.jump =0 : $scope.jump-= 25 ;
		$scope.loadMessages();
		console.log($scope.jump)
	}
 	$scope.exportToExcel=function(tableId){ // ex: '#my-table'
       /* var exportHref=Excel.tableToExcel(tableId,'sheet name');
            $timeout(function(){location.href=exportHref;},100); // trigger download*/
    }
}
function homeController($scope,$sails){
	$scope.room='1:1';
	$scope.tickets = {
		pendents : [] ,
		open : [] ,
		getPendents:function(){
			$sails.get('/ticket/getAreaTickets',{companyId:1,areaId:1})
					.success(function(data,status,headers,jwr){
						console.log('listando pendientes del área')
						var tokens=data.tickets;
						var length = tokens.length;
						var counter = 0;
		                $scope.user.getUserInfo(counter,length,tokens)
					})
					.error(function(data,status,headers,jwr){
						console.log(status);
					});
		},
		getOpenTickets:function(){
			$sails.get('/ticket/getMyTickets',{companyId:1,operatorId:1,areaId:1,web:'true'})
					.success(function(data,status,headers,jwr){
						console.log('obteniendo tickets');
						var tokens=data.tickets;
						var length = tokens.length;
						var counter = 0;
						$scope.user.getUserInfo(counter,length,tokens);					
					})
					.error(function(data,status,headers,jwr){
						console.log(status);
					})
		}
	};
	$scope.user={
		getUserInfo:function(counter,length,tokens){
			if(counter == length) return;
			var self = tokens[counter];
			self.messages=[];
			console.log('pendientes')
			$sails.get('/user/getUserInfo',{userId:self.user})
					.success(function(data,status,headers,jwr){
		            	self.userInfo=data.user;
						//uniendolo al room del ticket para que reciba notificaciones
						$sails.get('/message/join',{room:self.id})
						        .success(function(data,status,headers,jwr){
						                console.log(data);
						              })
						        .error(function(data,status,headers,jwr){
						        	console.log(status);
						        })
						$sails.get('/message/gmft',{ticket:self.id})
						        .success(function(messages,status,headers,jwr){
						            self.messages=messages.tickets;
						            //agregando la data a pendientes
						            if(self.status == 0) {
						            	$scope.tickets.pendents.push(self);
						            }else{
						            	$scope.tickets.open.push(self);
						            }
						            
						        })
						       	.error(function(data,status,headers,jwr){
						       		console.log(status);
						       	}) 
					}).error(function(data,status,headers,jwr){
						console.log(status);
					})
			counter++;
			$scope.user.getUserInfo(counter,length,tokens)
		}
	}
	$scope.joinArea=function(){
		$sails.get('/ticket/join',{room:$scope.room},function(data){
		                console.log('uniendose al área ' + $scope.room);
		                console.log(data.msg);
					});		
	}
	//lauching
	$scope.joinArea();
	$scope.tickets.getPendents();
	$scope.tickets.getOpenTickets();
}

$(document).on('ready',init);

function init(){
	resize();
	setHeight()
	$(window).resize(function(){
		resize();
		setHeight()
	});
}

function resize(){
	$('.history,.agents').css({
		'width':(window.innerWidth - 180)+'px'
	})
	var w = window.innerWidth;
	$('#operator-home .open').css({
		'width':(w-290)+'px'
	});
}
function setHeight(){
	var h = window.innerHeight;
	$('#operator-home .in-comming ul').css({
		'height':(h-76)+'px'
	});
	$(".chat-open").css({
		'height':(h-30 -76 )+'px'
	});
	$("#operator-home .open > .elements .chat-open").css({
		//'margin-top': '-'+(h-103)+'px'
		'margin-top':'0px'
	});
	$("#operator-home .open > .elements .chat-open .messages").css({
		'height':(h-310)+'px'
	});
}
setTimeout(function(){
  setHeight();
},1000)