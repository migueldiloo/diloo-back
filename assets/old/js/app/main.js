define(["jquery","angular","analitics"],function($,$$){
  $("[data-component='chat-window'] .action").on('click',function(){
    var $action=$(this).attr('data-action');
    var $icon=$(this).attr('data-icon');
    var $parent = $(this).parent().parent();
    $parent.children(".h-card,[data-role='content-pane'],form").slideToggle('fast');
    if($action === 'minimize'){
      $(this).attr('data-action','maximize');
      $(this).attr('data-icon','maximize');

    }else{
      $(this).attr('data-action','minimize');
      $(this).attr('data-icon','minimize');
    }
  });
  $("[data-component='chat-window'] [data-action='close']").on('click',function(){
    var $parent = $(this).parent().parent();
    $($parent).fadeToggle('fast');
  });
  $("[data-component='search-bar'] button").hover(
    function(){
      var $el=$(this).parent();
      $el.children('input').fadeIn('fast').focus();
      $el.children('button').css({
        'border-radius':' 0 .5rem .5rem 0'
      });
    });
  $("[data-component='search-bar'] input").blur(
    function(){
      $(this).fadeOut('fast');
      $(this).parent().children('button').css({
        'border-radius':'50%'
      });
  });
  $("header .status .u-photo").on('click',function(){
    $("[data-component='user-menu']").slideToggle('fast');
  });
  $("input[type='radio']").on('click',function(){
    var val=$("input[type='radio']:checked").val();
    if(val == 1){
      $(".s").css({
        'background-color':'rgb(110, 220, 95)'
      });
    }else{
      $(".s").css({
        'background-color':'orange'
      });
    }
  });
});
