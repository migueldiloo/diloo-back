window.dilooApp = {
  companyId : 1,
  areaId:1,
  atendidos : [],
  pendientes : [],
  toArray :function(obj){
    var tokenArray = [] ;
    for(key in obj){
      tokenArray.push(obj[key]);
    }
    return tokenArray ;
  },
  renderPendientes:function(obj){
    $('[data-role="container-im"] ul').append('<li data-component="incomming-message" data-area="sales" data-key="'+obj.key+
      '"><div class="chat-info"><p>'+obj.chats[0].author+'</p><p>'+obj.chats[0].message+'</p></div><div class="arrow-side"></div></li>')
  },
  renderChat:function(obj,other){
      var key = obj.key();
      var chats = window.dilooApp.toArray(obj.val().chat);
      var dom = "";
      for(var i =0 ;i< chats.length;i++){
        if(chats[i].type ==='SENDER'){
          var classCss = 'sender';
        }else{
          var classCss = 'reciber';
        }
        dom +="<li><p class='"+classCss+"'>"+chats[i].author+"</p><p class='message'>"+chats[i].message+"</p></li>"
      }
      //console.log(other)
      if(typeof other === 'object'){
         return dom;
      }else {
        $("ol[data-key='"+key+"']").html(dom)   
      }
  },
  renderChatWindow:function(key,chatList){
      $(".chat-windows").append('<div data-component="chat-window" data-window-key="'+key+'"><header>'+
                              '<p data-role="ticket-code"> Ticket 00001</p>'+
                              //'<button class="action" data-action="minimize" data-icon="minimize"></button>'+
                              '</header>'+
                              '<div class="h-card"><div><p class="p-name">Usuario</p><p>'+
                              '<span data-icon="phone"></span><span class="p-tel">940610789</span></p></div></div>'+
                              '<div data-role="content-pane"><ul data-role="tabs">'+
                                '<li class="active"><a href="#chat">Chat</a></li>'+
                              '</ul>'+
                              '<div id="chat" data-role="pane" data-status="active">'+
                              '<ol data-key="'+key+'">'+
                              chatList +
                              '</ol>'+
                              '</div>'+
                              '</div>'+
                              '<form data-role="send-message" data-key="'+key+'">'+
                                '<textarea placeholder="mensaje" name="message"></textarea>'+
                              '</form>'+
                              '<footer>'+
                                '<button data-action="close" data-key="'+key+'">Finalizar chat</button>'+
                                '<p>Pulsa "Enter" para enviar.</p>'+
                              '</footer>'+
                              '</div>')    
  }
}
$(document).on("ready",function(){
  //firebase ------------------
  diloo = new Firebase('https://diloo.firebaseio.com/'+window.dilooApp.companyId)

  //carga inicial
  diloo.once('value',function(snapshot){
    //mostrando los atendidos en caso de refresh del navegador
    var key;
    var chats;
    var dom;
    //console.log(window.dilooApp.atendidos)
    for(var i =0 ;i< window.dilooApp.atendidos.length ; i++){
      var key =window.dilooApp.atendidos[i].key;
      var dom = '';
      var chats = window.dilooApp.atendidos[i].chats;
      for(var j =0 ;j< chats.length;j++){
        dom +="<li><p class='from'>"+chats[j].author+"</p><p class='message'>"+chats[j].message+"</p></li>"
      }      
      window.dilooApp.renderChatWindow(key,dom);
      //height();
    }  
    
  });
  //push y update
  $("body").delegate("textarea","keydown",function(e){
    window.eve=e;
    if( e.which == 13){
      var val = $(this).val();
      var url = $(this).parent().attr('data-key');
      var data = {
        author:'OPERATOR',
        date:'date',
        message:val,
        type:'RECIBER'
      };
      //pushing
      diloo.child(url + '/chat' ).push(data);
      diloo.child(url).update({responsed:true});
      $(this).val("");
    }
  });
  //obteniendo actualizaciones del chat
  diloo.on('child_changed',function(snapshot){
    window.dilooApp.renderChat(snapshot,true);
  });
//cargando pendientes
  diloo.on('child_added',function(snapshot){
    var token = snapshot.val();
    token.key = snapshot.key();
    if(token.responsed === true){
      token.chats = window.dilooApp.toArray(token.chat);
      delete token.chat;
      window.dilooApp.atendidos.push(token);
    }else if(token.responsed !== 'closed'){
      token.chats = window.dilooApp.toArray(token.chat);
      delete token.chat;
      window.dilooApp.renderPendientes(token);
      window.dilooApp.pendientes.push(token);
    }  
    $('[data-role="container-im"] .counter p').text( window.dilooApp.pendientes.length);
    $('[data-role="container-chat"] .counter p').text( window.dilooApp.atendidos.length);
  }); 
  //mostrando atendidos
  $('[data-role="container-im"] ul').delegate("[data-component='incomming-message']",'click',function(e){
    e.preventDefault();
    var key = $(this).attr('data-key');
    var token = new Firebase('https://diloo.firebaseio.com/'+window.dilooApp.companyId+'/'+key);
    token.once('value',function(snapshot){
      var chatList=window.dilooApp.renderChat(snapshot,null); //dom elements
      window.dilooApp.renderChatWindow(key,chatList);
      //height();
      })
      $(this).fadeOut();
    });
  //finalizando chat
  $("body").delegate('[data-action="close"]','click',function(e){
      e.preventDefault();
      var key = $(this).attr('data-key');
      /*diloo.child(key).update({
        'responsed':'closed'
      });
      $(this).parent().parent().fadeOut();*/
      $("[data-component='close-chat']").attr('data-key',key);
      $("[data-component='close-chat']").fadeIn();
  });
  $('[data-component="close-chat"] .exit').on('click',function(){
    $('[data-component="close-chat"]').fadeOut();
  });
  $('[data-component="close-chat"] .end').on('click',function(){
      var key = $('[data-component="close-chat"]').attr('data-key');
      diloo.child(key).update({
        'responsed':'closed'
      });
      $("[data-window-key='"+key+"']").fadeOut();
      $('[data-component="close-chat"]').fadeOut();
      //$('[data-component="chat-window"] ')
  }); 
//end---------------------------
  $("body").delegate("[data-component='chat-window'] .action",'click',function(){
    var $action=$(this).attr('data-action');
    var $icon=$(this).attr('data-icon');
    var $parent = $(this).parent().parent();
    $parent.children(".h-card,[data-role='content-pane'],form").slideToggle('fast');
    if($action === 'minimize'){
      $(this).attr('data-action','maximize');
      $(this).attr('data-icon','maximize');

    }else{
      $(this).attr('data-action','minimize');
      $(this).attr('data-icon','minimize');
    }
  });
  $("[data-component='search-bar'] button").hover(
    function(){
      var $el=$(this).parent();
      $el.children('input').fadeIn('fast').focus();
      $el.children('button').css({
        'border-radius':' 0 .5rem .5rem 0'
      });
    });
  $("[data-component='search-bar'] input").blur(
    function(){
      $(this).fadeOut('fast');
      $(this).parent().children('button').css({
        'border-radius':'50%'
      });
  });
  $("header .status .u-photo").on('click',function(){
    $("[data-component='user-menu']").slideToggle('fast');
  });
  $("input[type='radio']").on('click',function(){
    var val=$("input[type='radio']:checked").val();
    if(val == 1){
      $(".s").css({
        'background-color':'rgb(110, 220, 95)'
      });
    }else{
      $(".s").css({
        'background-color':'orange'
      });
    }
  });
  $("#login form").on('submit',function(e){
    e.preventDefault();
    $.ajax({
      url:$(this).attr('action'),
      method:$(this).attr('method'),
      data:$(this).serialize()
    }).complete(function(data){
      if(data.responseJSON.err){
        alert(data.responseJSON.err)
      }else{
        window.location.href= '/operator/home'
      }
    })
  })
  /*$(window).on('resize',function(){
    height();
  });*/
  /*function height(){
    var height = window.innerHeight;
    $("[data-role='pane']").css({
      'height': (height-480)+'px'
    });
  }*/
});