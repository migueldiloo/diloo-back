window.dilooApp = {
  operatorId:1,
  companyId : 9,
  areaId:1,
  tickets : [],
  companyData:{
    "areas": [],
    "supervisors": [],
    "operators": [],
    "category": {
      "id": 2,
      "name": "Seguros",
      "status": true,
      "createdAt": "2015-05-28T19:36:51.000Z",
      "updatedAt": "2015-05-28T19:36:51.000Z"
    },
    "id": 1,
    "name": "Saga",
    "image": "http://1.bp.blogspot.com/-AVVV9V9UqkU/UZuywn64u8I/AAAAAAAAAD8/YOhnQj9cGXA/s1600/imagen falabella.jpg",
    "bgImage": "http://1.bp.blogspot.com/-AVVV9V9UqkU/UZuywn64u8I/AAAAAAAAAD8/YOhnQj9cGXA/s1600/imagen falabella.jpg",
    "description": "falabella",
    "id": 9,
    "name": "MAPFRE",
    "image": "/companyLogos/mapfre.jpg",
    "bgImage": "/companyLogos/mapfre.jpg",
    "description": "Entidad aseguradora",
    "status": true,
    "createdAt": "2015-05-28T19:43:16.000Z",
    "updatedAt": "2015-05-28T19:43:16.000Z"
  },
  clearBox:function(key){
    console.log('in')
    $('[data-window-key="'+key+'"] input[type="text"]').val('');
  },
  renderPendientes:function(obj){
    var message = obj.messages[obj.messages.length - 1]  || 'Nuevo mensaje'
    $('[data-role="container-im"] ul').append('<li data-component="incomming-message" data-area="sales" data-key="'+obj.id+
      '"><div class="chat-info"><p>'+obj.userInfo.nombres+'</p><p>'+message+'</p></div><div class="arrow-side"></div></li>')
  },
  renderChat:function(key,other){
      var key = key;
      var chats = [];
      for(var i = 0;i<window.dilooApp.tickets.length;i++){
        if(window.dilooApp.tickets[i].id == key ){
          chats=window.dilooApp.tickets[i].messages;
        }
      }
      console.log(chats)
      var dom = "";
      for(var i =0 ;i< chats.length;i++){
        if(chats[i].type =='SENDER'){
          var classCss = 'sender';
        }else{
          var classCss = 'reciber';
        }
        dom +="<li><p class='"+classCss+"'>"+chats[i].author+"</p><p class='message'>"+chats[i].body+"</p></li>"
      }
      console.log(dom);
      if(typeof other === 'object'){
         return dom;
      }else {
        $("ol[data-key='"+key+"']").html(dom)   
      }
  },
  renderChatWindow:function(key,chatList){
      var userInfo ;
      for(var i=0;i<window.dilooApp.tickets.length;i++){
        var token= window.dilooApp.tickets[i];
        if(token.id == key){
          userInfo = token.userInfo;
        }
      }
      console.log(userInfo)
      $(".chat-windows").append('<div data-component="chat-window" data-window-key="'+key+'"><header>'+
                              '<p data-role="ticket-code"> Ticket# '+key+'</p>'+
                              //'<button class="action" data-action="minimize" data-icon="minimize"></button>'+
                              '</header>'+
                              '<div class="h-card">'+
                                '<div><p class="p-name">'+userInfo.nombres+'</p><p>'+
                                  '<span data-icon="phone"></span><span class="p-tel">'+userInfo.phone+'</span></p>'+
                                  '<div>'+userInfo.email+'</div>'+
                                '</div>'+
                                '<div><img src="'+userInfo.photo+'" /></div>'+
                              '</div>'+
                              '<div data-role="content-pane"><ul data-role="tabs">'+
                                '<li class="active"><a href="#chat">Chat</a></li>'+
                              '</ul>'+
                              '<div id="chat" data-role="pane" data-status="active" class="window" data-key="'+key+'">'+
                              '<ol data-key="'+key+'">'+
                              chatList +
                              '</ol>'+
                              '</div>'+
                              '</div>'+
                              '<form data-role="send-message" data-key="'+key+'">'+
                                '<input style="width:100%;"  type="text" placeholder="mensaje" name="message" />'+
                              '</form>'+
                              '<footer>'+
                                '<button data-action="close" data-key="'+key+'">Finalizar chat</button>'+
                                '<p>Pulsa "Enter" para enviar.</p>'+
                              '</footer>'+
                              '</div>') ;

  }  
}
var room= window.dilooApp.companyId+':'+window.dilooApp.areaId;
//obtengo mis tickets abiertos
io.socket.get('/ticket/getMyTickets',
              {companyId:window.dilooApp.companyId,
                operatorId:window.dilooApp.operatorId,
                areaId:window.dilooApp.areaId,
                web:'true'
              },
              function(data){
                console.log('obteniendo tickets');
                var tokens=data.tickets;
                for(var i = 0;i<tokens.length;i++){
                    var item = tokens[i];
                    item.messages=[];
                    io.socket.get('/user/getUserInfo',
                                  {
                                    userId:item.user
                                },
                                function(data){
                                  //var chatList=window.dilooApp.renderChat(tokens[i].id,null); //dom elements
                                  item.userInfo=data.user;
                                  window.dilooApp.renderChatWindow(item.id,'')
                                  io.socket.get('/message/gmft',
                                            {
                                              ticket:item.id
                                            },
                                            function(msgs){
                                                 var len = msgs.tickets.length;
                                                 for(var k=0;k<len;k++){
                                                    var ntoken = msgs.tickets[k];
                                                    item.messages.push(ntoken);
                                                    $('ol[data-key="'+item.id+'"]').append("<li><p class='"+ntoken.type+"'>"+ntoken.author+"</p><p class='message'>"+ntoken.body+"</p></li>");
                                                 }
                                            }
                                  );
                                }
                    );
                  window.dilooApp.tickets.push(item);  
                }         
              }
);
//obtengo mis tickets pendientes
io.socket.get('/ticket/getAreaTickets',
              {companyId:window.dilooApp.companyId,
                areaId:window.dilooApp.areaId
              },
              function(data){
                console.log('listando pendientes del área')
                var tokens=data.tickets;
                for(var i = 0;i<tokens.length;i++){
                    var self = tokens[i];
                    self.messages=[];
                    //consultando la info del usuario
                    io.socket.get('/user/getUserInfo',
                                  {
                                    userId:self.user
                                },
                                function(data){
                                  self.userInfo=data.user;
                                  //uniendolo al room del ticket para que reciba notificaciones
                                  io.socket.get('/message/join',
                                                {
                                                  room:self.id
                                                },
                                                function(m2){
                                                  console.log(m2.msg);
                                                }
                                  );   
                                  //obtengo los mensajes de los tickets pendientes
                                  io.socket.get('/message/gmft',
                                                {
                                                  ticket:self.id
                                                }
                                                ,function(messages){
                                                  self.messages=messages.tickets;
                                                }
                                  );
                                  //agregando la data a pendientes
                                  window.dilooApp.tickets.push(self);
                                  window.dilooApp.renderPendientes(self);                             
                                }
                    )

                }
              }
);
//uniendolo a un room
io.socket.get('/ticket/join',
              {
                room:room
              },
              function(data){
                console.log('uniendose al área ' + room)
                console.log(data.msg);
              }
);
//recibiendo tickets nuevos
io.socket.on('newticket',function(data){
              var ticket = data.ticket;
              ticket.messages=[];
              //console.log(data)
              io.socket.get('/user/getUserInfo',
                          {
                            userId:ticket.user
                        },
                        function(userInfo){
                          ticket.userInfo=userInfo.user;
                          //agregando la data a pendientes
                          window.dilooApp.tickets.push(ticket);
                          window.dilooApp.renderPendientes(ticket);
                          //uniendolo al room del ticket para que reciba notificaciones
                          io.socket.get('/message/join',
                                        {
                                          room:ticket.id
                                        },
                                        function(m2){
                                          console.log(m2);
                                        }
                          );                                  
                        }
              )

});
//nuevo mensaje
io.socket.on('msg',function(data){
  console.log('nuevo mensaje');
  console.log(data);
  $("[data-key="+data.ticket+"] .chat-info p:eq(1)").text(data.msg);
  var len = window.dilooApp.tickets.length;
  for(var i =0;i<len;i++){
    var token = window.dilooApp.tickets[i];
    if(token.id == data.ticket){
      window.dilooApp.tickets[i].messages.push(data);
      $('ol[data-key="'+data.ticket+'"]').append("<li><p class='"+data.type+"'>"+data.author+"</p><p class='message'>"+data.body+"</p></li>");
    }
  }
  var dom =$(".window[data-key='"+data.ticket+"']");
  dom.scrollTop(dom.height());  
});
//-------------------metodos del dom
//mostrar ventana de chat
$('[data-role="container-im"] ul').delegate("[data-component='incomming-message']",'click',function(e){
  e.preventDefault();
  var key = $(this).attr('data-key');
  var chatList=window.dilooApp.renderChat(key,null); //dom elements
  window.dilooApp.renderChatWindow(key,chatList);
  $(this).fadeOut();
});
//respondermensajes
$("body").delegate("input[type='text']","keydown",function(e){
  if( e.which == 13){
  e.preventDefault();    
    var val = $(this).val();
    var url = $(this).parent().attr('data-key');
    var message = {
      author:'OPERATOR',
      body:val,
      type:'RECIBER',
      ticket:url,
      company:window.dilooApp.companyData
    };
    //pushing
    io.socket.get('/message/web',
                  message,
                  function(data){
                    console.log(data);
                    /*if(data.ksuccess ==true){
                      var len =window.dilooApp.tickets.length;
                      for(var i =0;i<len;i++){
                        var token=window.dilooApp.tickets[i];
                        if(token.id==url){
                          token.messages.push(message);*/
                          $('ol[data-key="'+url+'"]').append("<li><p class='"+message.type+"'>"+message.author+"</p><p class='message'>"+message.body+"</p></li>");
                       /* }
                      }
                    }*/
                  }
    );
    window.dilooApp.clearBox(url);
  }
});
//finalizando chat
$("body").delegate('[data-action="close"]','click',function(e){
    e.preventDefault();
    var key = $(this).attr('data-key');
    /*diloo.child(key).update({
      'responsed':'closed'
    });
    $(this).parent().parent().fadeOut();*/
    $("[data-component='close-chat']").attr('data-key',key);
    $("[data-component='close-chat']").fadeIn();
});
$('[data-component="close-chat"] .exit').on('click',function(){
  $('[data-component="close-chat"]').fadeOut();
});
$('[data-component="close-chat"] .end').on('click',function(){
    var key = $('[data-component="close-chat"]').attr('data-key');

    io.socket.get('/ticket/closeTicket',
                  {
                    ticketId:key
                  },
                  function(data){
                    console.log(data)
                  }
    );
    $("[data-window-key='"+key+"']").fadeOut();
    $('[data-component="close-chat"]').fadeOut();
    //$('[data-component="chat-window"] ')
	$("#home input[type='text']").css({'width':'100%'})
}); 

//css ----



